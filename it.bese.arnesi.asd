;;;; -*- lisp -*-

(defpackage :it.bese.arnesi.system
  (:use :common-lisp 
	:asdf))

(in-package :it.bese.arnesi.system)

(defsystem :it.bese.arnesi
    :version "0.6.1"
    :components ((:static-file "it.bese.arnesi.asd")
                 (:module :src
                          :components ((:file "accumulation" :depends-on ("packages" "macros"))
                                       (:static-file "arnesi.el")
                                       (:file "debug" :depends-on ("packages"))
                                       (:file "file-system" :depends-on ("packages"))
                                       (:file "flow-control" :depends-on ("packages"))
                                       (:file "hash" :depends-on ("packages" "list"))
                                       (:file "http" :depends-on ("packages" "vector"))
                                       (:file "lambda" :depends-on ("packages"))
                                       (:file "list" :depends-on ("packages"))
                                       (:file "log" :depends-on ("packages"))
                                       (:file "macros" :depends-on ("packages"))
                                       (:file "numbers" :depends-on ("packages"))
                                       (:file "packages")
                                       (:file "sequence" :depends-on ("packages"))
                                       (:file "string" :depends-on ("packages"))
                                       (:file "symbol" :depends-on ("packages"))
                                       (:file "vector" :depends-on ("packages")))))
    :description
    "Collection of various bits and pieces used in various projects.")
