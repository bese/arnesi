;;;; -*- lisp -*-
;;;; $Id: flow-control.lisp,v 1.3 2003/08/16 15:39:19 mb Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.arnesi)

(defmacro while (condition &body body)
  "Evaluate BODY as long as CONDITION is true."
  `(do ()
       ((not ,condition))
     ,@body))

(defmacro until (condition &body body)
  "Evaluate BODY until CONDITION returns true, in other words, as long
  as CONDITION returns false"
  `(do ()
       (,condition)
     ,@body))

(defmacro if-bind (symbol condition then &optional else)
  "Bind SYMBOL to the value(s) of CONDITION. If the first value
  returned by CONDITION is true then evaluate THEN, otherwise evaluate
  ELSE. SYMBOL can be a symbol, in which case it is bound to the first
  value of CONDITION, or a list in which case it is bound ala
  MULTIPLE-VALUE-BIND."
  (if (listp symbol)
      `(multiple-value-bind ,symbol ,condition
         (if ,(car symbol)
             ,then
             ,else))
      `(let ((,symbol ,condition))
         (if ,symbol
             ,then
             ,else))))

(defmacro when-bind (symbol condition &body body)
  "Bind SYMBOL to the return value(s) of CONDITION. If the first value
  returned by CONDITION is true evaluate the body ala WHEN. If SYMBOL
  is a list it is bound to condition via MULTIPLE-VALUE-BIND."
  (if (listp symbol)
      `(multiple-value-bind ,symbol ,condition
         (when ,(car symbol)
             ,body))
      `(let ((,symbol ,condition))
         (when ,symbol
           ,@body))))

(defmacro cond-bind (symbol &rest clauses)
  "CLAUSES are evaluated like COND, but SYMBOL is bound (like with
  BIF) to the return value(s) for the life of the clause."
  (when clauses
    `(bif ,symbol ,(caar clauses)
	(progn ,@(cdar clauses))
        (bcond ,symbol ,@(cdr clauses)))))

(defmacro case-bind (var form &rest case-body)
  `(let ((,var ,form))
     (case ,var ,@case-body)))

(defmacro while-bind (var condition &body body)
  "Like WHILE, but VAR is bound to value of CONDITION on each
  iteration of BODY. Unlike BIF multiple values are ignored."
  `(do ((,var ,condition ,condition))
       ((not ,var))
     ,@body))

(defmacro until-bind (var condition &rest body)
  "Provided for the hell of it. VAR is bound to NIL on each iteration
  of the loop."
  `(do ((,var ,condition ,condition))
       (,var)
     ,@body))

(defmacro whichever (&rest possibilities)
  "Evaluates one (and only one) of its args, which one is chosen at random"
  `(ecase (random ,(length possibilities))
     ,@(loop for poss in possibilities
             for x from 0
             collect (list x poss))))

(defmacro xor (&rest datums)
  "Evaluates the args one at a time. If more than one arg returns true
  evaluation stops and NIL is returned. If exactly one arg returns
  true that value is retuned."
  (let ((state (gensym "XOR-state-"))
        (block-name (gensym "XOR-block-"))
        (arg-temp (gensym "XOR-arg-temp-")))
    `(let ((,state nil)
           (,arg-temp nil))
       (block ,block-name
         ,@(loop
              for arg in datums
              collect `(setf ,arg-temp ,arg)
              collect `(if ,arg-temp
                           ;; arg is T, this can change the state
                           (if ,state
                               ;; a second T value, return NIL
                               (return-from ,block-name nil)
                               ;; a first T, swap the state
                               (setf ,state ,arg-temp))))
         (return-from ,block-name ,state)))))

(defmacro switch ((obj &key (test #'eql)) &rest clauses)
  ;; NB: There is no need to do the find-if and the remove here, we
  ;; can just as well do them with in the expansion
  (let ((default-clause (find-if (lambda (c) (eq t (car c))) clauses)))
    (when default-clause
      (setf clauses (remove default-clause clauses :test #'equalp)))
    (let ((obj-sym (gensym))
          (test-sym (gensym)))
      `(let ((,obj-sym ,obj)
             (,test-sym ,test))
         (cond
           ,@(mapcar (lambda (clause)
                       (let ((key (car clause))
                             (form (cdr clause)))
                         `((funcall ,test-sym ,obj-sym ,key) ,@form)))
                     clauses)
           ,@(when default-clause
                   `((t ,@(cdr default-clause)))))))))

(defmacro eswitch ((obj &key (test #'eql)) &rest body)
  (let ((obj-sym (gensym)))
    `(let ((,obj-sym ,obj))
       (switch (,obj-sym :test ,test)
               ,@body
               (t (error "Unmatched SWITCH. Testing against ~A." ,obj-sym))))))

(defmacro cswitch ((obj &key (test #'eql)) &rest body)
  (let ((obj-sym (gensym)))
    `(let ((,obj-sym ,obj))
       (switch (,obj-sym :test ,test)
               ,@body
               (t (cerror "Unmatched SWITCH. Testing against ~A." ,obj-sym))))))
