;;;; -*- lisp -*-
;;;; $Id: list.lisp,v 1.1 2003/08/16 15:56:43 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.arnesi)

(defmacro dolist* ((iterator list &optional return-value) &body body)
  (if (listp iterator)
      (let ((i (gensym "DOLIST*-i-")))
        `(dolist (,i ,list ,return-value)
           (destructuring-bind ,iterator ,i
             ,@body)))
      `(dolist (,iterator ,list ,return-value)
         ,@body)))

(defun ensure-list (thing)
  (if (listp thing)
      thing
      (list thing)))

(defun ensure-cons (thing)
  (if (consp thing)
      thing
      (cons thing nil)))

(defun partition (list &rest lambdas)
  (let ((collectors (mapcar (lambda (l)
                              (cons (if (string= 'otherwise l)
                                        (constantly t)
                                        l)
                                    (make-collector)))
                            lambdas)))
    (dolist (item list)
      (dolist* ((test-func collector-func) collectors)
        (when (funcall test-func item)
          (funcall collector-func item))))))

(defun partitionx (list &rest lambdas)
  (let ((collectors (mapcar (lambda (l)
                              (cons (if (string= 'otherwise l)
                                        (constantly t)
                                        l)
                                    (make-collector)))
                            lambdas)))
    (dolist (item list)
      (block item
        (dolist* ((test-func collector-func) collectors)
          (when (funcall test-func item)
            (funcall collector-func item)
            (return-from item)))))))

(defmacro do-tree ((name tree &optional ret-val) &body body)
  (with-unique-names (traverser list list-element)
    `(progn
       (labels ((,traverser (,list)
                  (dolist (,list-element ,list)
                    (if (consp ,list-element)
                        (,traverser ,list-element)
                        (funcall (lambda (,name) ,@body) ,list-element)))))
         (,traverser ,tree)
         ,ret-val))))

(define-modify-macro push* (&rest items)
  (lambda (list &rest items)
    (dolist (i items)
      (setf list (cons i list)))
    list)
  "Pushes every element of ITEMS onto LIST. Equivalent to calling PUSH
  with each element of ITEMS.")
