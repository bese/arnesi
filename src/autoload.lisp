;;;; -*- lisp -*-

(in-package :it.bese.arnesi)

(defun make-autoloader (function-name file-name if-file-newer compile)
  (flet ((compile&load ()
           (if compile
	       (load (compile truename))
	       (load truename))))
    (let ((loaded nil)
	  (last-loaded-time 0))
      (lambda (&rest args)
	(if-bind truename (probe-file file-name)
	  (if loaded
	      (error "Trying to autoload ~S from ~S which doesn't define ~S" function-name file-name function-name)
	    (ecase if-file-newer
	      (:ignore
	       (compile&load)
	       (apply (symbol-function function-name) args))
	      (:reload
	       (let ((write-date (file-write-date truename)))
		 (when (or (null write-date)
			   (< last-loaded-time write-date))
		   ;; need a reload
		   (compile&load))
		 (prog1
		     (apply (symbol-function function-name) args)
		   (setf (symbol-function function-name) *SELF*)
		   (setf last-loaded-time (1+ write-date))))))
	    (error "Trying to autoload ~S from non existant file ~S" function-name file-name)))))))

(defmacro def-autoload (function-name file-name &key if-file-newer compile)
  `(defun ,function-name (&rest args)
     (apply (make-autoloaded ',function-name ,file-name ,if-file-newer ,compile)
	    args)))
