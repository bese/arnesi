;;;; -*- lisp -*-
;;;; $Id: lambda.lisp,v 1.2 2003/08/03 17:21:13 mb Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.arnesi)

(defun compose (f1 &rest functions)
  (case (length functions)
    (0 f1)
    (1 (lambda (&rest args)
         (funcall f1 (apply (car functions) args))))
    (2 (lambda (&rest args)
         (funcall f1
                  (funcall (first functions)
                           (apply (second functions) args)))))
    (3 (lambda (&rest args)
         (funcall f1
                  (funcall (first functions)
                           (funcall (second functions)
                                    (apply (third functions) args))))))
    (t
     (let ((funcs (nreverse (cons f1 functions))))
       (lambda (&rest args)
         (loop
            for f in funcs
            for r = (apply f args) then (funcall f r)
            finally (return r)))))))

(defun conjoin (&rest predicates)
  (case (length predicates)
    (0 (error "Must pass at least one predicate to CONJOIN"))
    (1 (car predicates))
    (2 (lambda (&rest args)
         (and (apply (first predicates) args)
              (apply (second predicates) args))))
    (3 (lambda (&rest args)
         (and (apply (first predicates) args)
              (apply (second predicates) args)
              (apply (third predicates) args))))
    (t
     (lambda (&rest args)
       (loop
          for p on predicates
          if (null (cdr p))
            return (apply (car p) args)
          else
            unless (apply (car p) args)
              return nil)))))

(defun curry (function &rest initial-args)
  (lambda (&rest args)
    (apply function (append initial-args args))))

(defun y (lambda)
  (funcall (lambda (f)
             (funcall (lambda (g)
                        (funcall g g))
                      (lambda (x)
                        (funcall f
                                 (lambda ()
                                   (funcall x x))))))
           lambda))
