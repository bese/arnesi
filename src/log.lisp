;;;; -*- lisp -*-
;;;; $Id: log.lisp,v 1.2 2003/08/10 09:32:45 mb Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.arnesi)

(defconstant +dribble+ 0)
(defconstant +debug+   1)
(defconstant +info+    2)
(defconstant +warn+    3)
(defconstant +error+   4)
(defconstant +fatal+   5)

(defclass log-category ()
  ((ancestors :initform '()     :accessor ancestors :initarg :ancestors)
   (handlers  :initform '()     :accessor handlers  :initarg :handlers)
   (level     :initform +debug+ :accessor log.level :initarg :level)))

(defmethod enabled-p ((cat log-category) level)
  (>= level (effective-level cat)))

(defmethod effective-level ((cat log-category))
  (if (log.level cat)
      (log.level cat)
      (if (ancestors cat)
          (let ((min +dribble+))
            (dolist (ancestor (ancestors cat))
              (minf min (effective-level ancestor))))
          (error "Can't determine level for ~S" cat))))

(defmethod handle ((cat log-category) message)
  (dolist (handler (handlers cat))
    (handle handler message))
  (dolist (ancestor (ancestors cat))
    (handle ancestor message)))

(defclass log-handler ()
  ())

(defgeneric handle (log-handler message))

(defclass stream-log-handler ()
  ((stream :initarg :stream :accessor log-stream)))

(defmethod handle ((s stream-log-handler) message)
  (princ message (log-stream s))
  (terpri (log-stream s)))

(defclass file-log-handler (stream-log-handler)
  ())

(defmacro log.dribble (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +dribble+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro log.debug (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +debug+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro log.info (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +info+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro log.warn (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +warn+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro log.error (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +error+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro log.fatal (category message-control &rest message-args)
  (let ((cat (gensym)))
    `(let ((,cat ,category))
       (when (enabled-p ,cat +fatal+)
         (handle ,cat (format nil ,message-control ,@message-args))))))

(defmacro deflogger (name ancestors &rest options)
  (let (level handlers)
    (loop for option in options
         when (and (consp option)
                 (eql :level (car option)))
           do (setf level (second option))
         when (and (consp option)
                   (eql :handler (car option)))
           do (push (second option) handlers))
    `(progn
       (defparameter ,name (make-instance 'log-category
                                    :level ,level
                                    :handlers (list ,@handlers)
                                    :ancestors (list ,@ancestors)))
       (defmacro ,(intern (strcat name '#:.dribble) (symbol-package name)) (message-control &rest message-args)
         `(log.dribble ,',name ,message-control ,@message-args))
       (defmacro ,(intern (strcat name '#:.debug) (symbol-package name)) (message-control &rest message-args)
         `(log.debug ,',name ,message-control ,@message-args))
       (defmacro ,(intern (strcat name '#:.info) (symbol-package name)) (message-control &rest message-args)
         `(log.info ,',name ,message-control ,@message-args))
       (defmacro ,(intern (strcat name '#:.warn) (symbol-package name)) (message-control &rest message-args)
         `(log.warn ,',name ,message-control ,@message-args))
       (defmacro ,(intern (strcat name '#:.error) (symbol-package name)) (message-control &rest message-args)
         `(log.error ,',name ,message-control ,@message-args))
       (defmacro ,(intern (strcat name '#:.fatal) (symbol-package name)) (message-control &rest message-args)
         `(log.fatal ,',name ,message-control ,@message-args)))))
