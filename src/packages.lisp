;;;; -*- lisp -*-
;;;; $Id: packages.lisp,v 1.1 2003/08/16 15:56:43 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :common-lisp-user)

(defpackage :it.bese.arnesi
  (:nicknames :arnesi)
  (:use :common-lisp)
  (:export

   #:make-reducer
   #:make-pusher
   #:make-collector
   #:with-reducer
   #:with-collector
   
   #:ppm
   #:ppm1

   #:with-input-from-file
   #:with-output-to-file

   #:while
   #:until
   #:if-bind
   #:when-bind
   #:cond-bind
   #:case-bind
   #:while-bind
   #:until-bind
   #:whichever
   #:xor
   #:switch
   #:eswitch
   #:cswitch

   #:build-hash-table

   #:escape-as-uri
   #:unescape-as-uri
   #:nunescape-as-uri
   #:escape-as-html
   
   #:compose
   #:conjoin
   #:curry
   #:y

   #:dolist*
   #:dotree
   #:ensure-list
   #:ensure-cons
   #:partition
   #:partitionx
   #:push*

   #:log-category
   #:stream-log-handler
   #:file-log-handler
   #:log.dribble
   #:log.debug
   #:log.info
   #:log.warn
   #:log.error
   #:log.fatal
   #:deflogger
   #:+dribble+
   #:+debug+
   #:+info+
   #:+warn+
   #:+error+
   #:+fatal+
   #:log.level
   
   #:with-unique-names

   #:parse-ieee-double
   #:mulf
   #:divf
   #:minf
   #:maxf
   #:map-range
   #:do-range
   
   #:tail
   #:but-tail
   #:head
   #:but-head
   #:starts-with
   #:ends-with

   #:+lower-case-ascii-alphabet+
   #:+upper-case-ascii-alphabet+
   #:+ascii-alphabet+
   #:+alphanumeric-ascii-alphabet+
   #:+base64-alphabet+
   #:random-string
   #:strcat
   #:strcat*
   #:fold-strings
   #:~%
   #:~T
   #:+CR-LF+
   #:~D
   #:~A
   #:~S
   #:~W

   #:intern-concat

   #:vector-push-extend*

   #:with))
