;;;; -*- lisp -*-
;;;; $Id: matcher.lisp,v 1.4 2003/08/03 17:21:13 mb Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;;;;
;;;; extendable, backtracking, list structure matcher

;;;; The code is written in CPS style, it's hard to understand at
;;;; first but once you "get it" it's actually quite simple. Basically
;;;; the idea is that at every point during a match one of two things
;;;; can happen, the match can succedd or it can fail. What we do is
;;;; we pass every match two functions (closures usually), one which
;;;; specifies what to if it succedds and one which specifies what to
;;;; do if it fails. These two closures can refer to the original
;;;; matchs parameter and hence we can easily "backtrack" if we
;;;; fail. Another important aspect is that we explcitly pass the
;;;; target against which to match, if we didn't do this it would be
;;;; impossible to really backtrack.

;;;; symbol descriptions:

(in-package :it.bese.arnesi)

(defvar *match-handlers* (make-hash-table :test 'eql))

(defstruct (match-enviroment (:conc-name e.))
  target
  bindings)

(defmacro def-matcher (name args &body body)
  `(progn
     (setf (gethash ',name *match-handlers*)
	   (lambda ,args ,@body))
     ',name))

(defmacro def-matcher-macro (name args &body body)
  `(progn
     (setf (gethash ',name *match-handlers*)
	   (lambda ,args
	     (make-matcher (progn ,@body))))
     ',name))

(defun make-matcher (spec)
  (if (listp spec)
      (apply (gethash (car spec) *match-handlers* (lambda (&rest dummy)
						    (declare (ignore dummy))
						    (error "Don't know how to handle ~S" spec)))
	     (mapcar #'make-matcher (cdr spec)))
    (if (gethash spec *match-handlers*)
	;; we allow :X as a an abbreviation for (:x)
	(funcall (gethash spec *match-handlers*))
        spec)))

(defun match (matcher target)
  (funcall matcher (make-match-enviroment :target target :bindings '())
	   (lambda (e k q)
	     (declare (ignore k q))
	     (return-from match (values t (e.bindings e))))
	   (lambda (e k q)
	     (declare (ignore e k q))
	     (return-from match (values nil nil)))))

(def-matcher :group (matcher var)
  (lambda (e k q)
    (funcall matcher e
	     (lambda (e. k. q.)
	       (funcall k (make-match-enviroment :target (e.target e.)
						 :bindings (cons (cons var (loop with old-target = (e.target e)
										 until (eq old-target (e.target e.))
										 collect (pop old-target)))
								 (e.bindings e.)))
			k. q.))
	     q)))

(defmacro def-predicate-matcher (name call-args lambda)
  "Define a matcher which tries to see if the next element \"matches\"
the ARG. Should a return a one arg lambda which returns true if we
have a match and false otherwise. The lambda will be called with the
thing to test against as it's only arg"
  `(def-matcher ,name ,call-args
     (lambda (e k q)
       (if (funcall ,lambda (car (e.target e)))
	   (funcall k (make-match-enviroment :target (cdr (e.target e))
					     :bindings (e.bindings e))
		    k q)
	 (funcall q e k q)))))

(def-predicate-matcher :eql (object)
  (lambda (target)
    (eql target object)))

(def-predicate-matcher :equal (object)
  (lambda (target)
    (equal target object)))

(def-matcher-macro cl:quote (item)
  `(:eql ,item))

(def-matcher :e ()
  (lambda (e k q)
    (funcall k e k q)))

(def-matcher :sequence (a b)
  (lambda (e k q)
    (funcall a e
	     (lambda (e. k. q.)
	       (declare (ignore k.))
	       (funcall b e. k q.))
	     q)))

(def-matcher-macro :seq (&rest items)
  (case (length items)
    (0 `(:e))
    (1 (car items))
    (2 `(:sequence ,@items))
    (t `(:sequence ,(car items) (:seq ,@(cdr items))))))

(def-matcher :alternation (a b)
  (lambda (e k q)
    (funcall a e
	     (lambda (e. k. q.)
	       (declare (ignore k. q.))
	       ;; a has succedded we should go on this route, but if
	       ;; ever this route leads to failure we need to go back
	       ;; to b
	       (funcall k e. k (lambda (e.. k.. q..)
				      ;; if we get here then a matched
				      ;; but eventually lead to a
				      ;; failure
				   (declare (ignore e.. k.. q..))
				   (funcall b e k q))))
	     ;; a failed, try b
	     (lambda (e. k. q.)
	       (declare (ignore e. k. q.))
	       (funcall b e k q)))))

(def-matcher-macro :alt (&rest options)
  (case (length options)
    (0 (error "Can't handle alternativeless alternations yet."))
    (1 (car options))
    (2 `(:alternation ,@options))
    (t `(:alternation ,(car options) (:alt ,@(cdr options))))))

(def-matcher-macro :? (&rest items)
  `(:alt (:seq ,@items) (:e)))

(def-matcher-macro :?? (&rest items)
  `(:alt (:e) (:seq ,@items)))

(def-matcher :greedy-star (item)
  (labels ((make-greedy-star-matcher (item)
             (lambda (e k q)
	       (funcall item e
			(lambda (e. k. q.)
			  (declare (ignore q. k.))
			  (if (eq (e.target e) (e.target e.))
			      ;; item succedded by matching nothing,
			      ;; which is ok but we need to avoid the
			      ;; infinite loop this could create
			      (funcall k e. k (lambda (e.. k.. q..)
						  (declare (ignore e.. k.. q..))
						  (funcall k e k q)))
			    (funcall (make-greedy-star-matcher item) e. k
				     ;; item matched but than that led
				     ;; to a failure, backtrack and try
				     ;; without item
				     (lambda (e.. k.. q..)
				       (declare (ignore e.. k.. q..))
				       (funcall k e k q)))))
			;; item failed, just go on
			(lambda (e. k. q.)
			  (declare (ignore e. k. q.))
			  (funcall k e k q))))))
    (make-greedy-star-matcher item)))

(def-matcher-macro :* (&rest items)
  `(:greedy-star (:seq ,@items)))

(def-matcher-macro :+ (&rest items)
  `(:seq (:seq ,@items) (:* ,@items)))

(def-matcher :not (match)
  (lambda (e k q)
    (funcall match e q k)))

(def-matcher :positive-lookahead (match)
  (lambda (e k q)
    (funcall match e
	     (lambda (e. k. q.)
	       (declare (ignore e.))
	       (funcall k. e k q))
	     (lambda (e. k. q.)
	       (declare (ignore e.))
	       (funcall q. e k q)))))

#| FiveAM tests

 (defmacro matches (spec target)
   `(is-true (match (make-matcher ',spec) target)))

 (test simple
   (matches '(:e) '())
   (matches '(:e) '(a))
   (matches '(:eql a) '(a))
   (matches '('a) '(a))
   (matches `(:seq 'a 'b 'c) '(a b c))
   (matches `(:seq 
   (matches `(:seq 'a (:* b) 'a) '(a b b b b a b b a))