;;;; -*- lisp -*-
;;;; $Id: http.lisp,v 1.1 2003/08/16 15:56:43 mbaringer Exp $
;;;;
;;;; Copyright (c) 2002-2003, Edward Marco Baringer
;;;; All rights reserved. 
;;;; 
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions are
;;;; met:
;;;; 
;;;;  - Redistributions of source code must retain the above copyright
;;;;    notice, this list of conditions and the following disclaimer.
;;;; 
;;;;  - Redistributions in binary form must reproduce the above copyright
;;;;    notice, this list of conditions and the following disclaimer in the
;;;;    documentation and/or other materials provided with the distribution.
;;;;
;;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;;    of its contributors may be used to endorse or promote products
;;;;    derived from this software without specific prior written permission.
;;;; 
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :it.bese.arnesi)

(defvar *ok-set* '(#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m #\n #\o #\p
                   #\q #\r #\s #\t #\u #\v #\w #\x #\y #\z #\A #\B #\C #\D #\E #\F
                   #\G #\H #\I #\J #\K #\L #\M #\N #\O #\P #\Q #\R #\S #\T #\U #\V
                   #\W #\X #\Y #\Z #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))

(defun escape-as-uri (string)
  (with-output-to-string (escaped)
    (loop
       for char across string
       if (member char *ok-set*) do (princ char escaped)
       else do (format escaped "%~2,'0X" (char-code char)))))

(defun char->hex-value (char)
  (ecase char
    (#\0 0) (#\1 1) (#\2 2) (#\3 3) (#\4 4) (#\5 5) (#\6 6) (#\7 7) (#\8 8) (#\9 9)
    ((#\a #\A) 10)
    ((#\b #\B) 11)
    ((#\c #\C) 12)
    ((#\d #\D) 13)
    ((#\e #\E) 14)
    ((#\f #\F) 15)))

(defun make-escaped-table ()
  (let ((table (make-array '(16 16) :element-type 'character :initial-element #\\)))
    (dotimes (i 16)
      (dotimes (j 16)
        (setf (aref table i j) (code-char (+ (* i 16) j)))))
    table))

(defvar *unescape-table* (make-escaped-table))

(defun nunescape-as-uri (string)
  (if (= 0 (length string))
      string
      (loop for i upfrom 0 below (length string)
         for offset upfrom 0
         do (case (aref string i)
              (#\%
               (setf (aref string offset) (aref *unescape-table*
                                                (char->hex-value (aref string (incf i)))
                                                (char->hex-value (aref string (incf i))))))
              (#\+
               (setf (aref string offset) #\Space))
              (t
               (setf (aref string offset) (aref string i))))
         finally (return (subseq string 0 (1+ offset))))))
  
(defun unescape-as-uri (string)
  (let ((unescaped (copy-seq string)))
    (nunescape-uri unescaped)))

(defun make-html-entities ()
  (let ((ht (make-hash-table :test 'equal)))
    (setf (gethash #\<  ht) "&lt;"
	  (gethash #\>  ht) "&gt;"
	  (gethash #\&  ht) "&amp;"
	  (gethash #\" ht) "&quot;")
    ht))

(defvar *html-entites* (make-html-entities))

(defun escape-as-html (string)
  (with-output-to-string (escaped)
    (loop for char across string
       for &entity = (gethash char *html-entites*)
       if &entity do
         (princ &entity escaped)
       else if (> (char-code char) 127) do
         (format escaped "&#x~X;" (char-code char))
       else do
         (write-char char escaped))))
